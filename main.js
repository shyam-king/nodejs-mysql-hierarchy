(function () {
    const { promisify } = require("util");
    const mysql = require("mysql")

    // Interface to run raw queries
    class SQL {
        constructor(auth) {
            //TODO check if sql connection fails
            this.connection = mysql.createConnection(auth);
            this.query = promisify(this.connection.query).bind(this.connection);
        }
    }

    //SQLTypes properly supported and available for use
    const Types = require("./SQLTypes.json");

    // Represents a database in SQL
    class Database {
        constructor(name, dataObjects, sqlAuth, callback) {
            if (name === undefined)
                throw "name and type are required";
            if (dataObjects === undefined)
                throw "dataObjects is required";
            if (sqlAuth === undefined)
                throw "sqlAuth is required";

            this._name = name;
            this._dataObjects = dataObjects;
            this._initialised = false;
            this._sql = new SQL(sqlAuth);

            //TODO provide support to retain as much info as possible
            this._onSchemaUpdate = callback || ((d) => {
            });

            this._onTableCreate = (d) => {
            }
        }

        get name() {
            return this._name;
        }

        set name(name) {
            throw "Name of a Database cannot change once initialised";
        }

        setOnTableCreate(cb) {
            this._onTableCreate = cb;
        }

        async _init() {
            try {
                console.log("initializing database " + this._name + "...")
                await this._sql.query("USE " + this._name);
                await this._sql.query("CREATE TABLE IF NOT EXISTS _metadata (_tableName varchar(64), _version int);");

                await asyncForEach(this._dataObjects, async (dataObject) => {
                    console.log(`\tinitializing ${dataObject.name}...`);
                    if (await checkIfTableExists(this._sql, dataObject)) {
                        if (await checkIfTableSchemaMatches(this._sql, dataObject)) {
                            console.log(`\t\t${dataObject.name} is already up-to-date.`)
                        }
                        else {
                            //TODO apply more sophisticated schema updates
                            console.log(`\t\tupdating ${dataObject.name} with the new schema...`);
                            this._onSchemaUpdate(dataObject);
                            await this._sql.query("DROP TABLE " + dataObject._name);
                            await createTable(this._sql, dataObject);
                        }
                    }
                    else {
                        console.log(`\t\tcreating ${dataObject.name}...`);
                        this._onTableCreate(dataObject);
                        await createTable(this._sql, dataObject);
                    }

                });

                this._initialised = true;
            }
            catch (e) {
                throw e;
            }
        }

        async getInstance() {
            await this._init();
            console.log(this._name + " initialized.");
            return this;
        }
    }

    //Represents a column in SQL
    class Field {
        constructor(name, type) {
            if (type === undefined)
                throw "type needs to be specified";

            if (!(Object.values(Types).includes(type.toUpperCase())))
                throw `Provided type is not supported as a Field type`;

            if (name === undefined)
                throw "name and type are required";

            this._type = type;
            this._aliasName = name;
            this._name = "_" + name;

            this._isPrimaryKey = false;
            this._autoIncrement = false;
        }

        get name() {
            return this._aliasName;
        }

        set name(value) {
            throw "The name for a Field can be set only during it's initialisation.";
        }

        primaryKey(isPrimaryKey) {
            if (isPrimaryKey !== undefined)
                this._isPrimaryKey = isPrimaryKey;
            else
                this._isPrimaryKey = true;

            return this;
        }

        autoIncrement(shouldAutoIncrement) {
            if (shouldAutoIncrement !== undefined)
                this._autoIncrement = shouldAutoIncrement;
            else
                this._autoIncrement = true;

            return this;
        }

        default(defaultValue) {
            this._default = defaultValue;
            return this;
        }
    }

    //Represents a Table in SQL
    class DataObjectCollection {
        constructor(name, fields, version) {
            this._aliasName = name;
            this._fields = fields;
            this._name = "_" + name;
            this._version = version || 0;
        }

        get name() {
            return this._aliasName;
        }

        set name(name) {
            throw "The name for a DataObject can be set only during it's initialisation.";
        }
    }

    async function checkIfTableSchemaMatches(sql, dataObject) {
        let result = await sql.query(`SELECT _version FROM _metadata WHERE _tableName = "${dataObject._name}";`);
        return result[0]._version == dataObject._version;
    }

    async function checkIfTableExists(sql, dataObject) {
        let results = await sql.query("SHOW TABLES;");
        let found = false;
        results.forEach(element => {
            if (Object.values(element)[0] == dataObject._name) {
                found = true;
            }
        });

        return found;
    }

    async function createTable(sql, dataObject) {
        let query = `CREATE TABLE ${dataObject._name} (`;
        let primaryKeys = [];
        dataObject._fields.forEach((field, i, a) => {
            if (field._isPrimaryKey)
                primaryKeys.push(field._name);

            query += `${field._name} ${field._type}`

            if (field._autoIncrement) {
                query += " auto_increment";
            }

            if (field._default !== undefined)
                query += ` DEFAULT "${field._default}"`;

            if (i < a.length - 1)
                query += ", ";
        });

        if (primaryKeys.length != 0) {
            query += `, PRIMARY KEY(${primaryKeys.join(", ")})`;
        }

        query += ");";

        await sql.query(query);
        await sql.query(`DELETE FROM _metadata WHERE _tableName = "${dataObject._name}";`);
        await sql.query(`INSERT INTO _metadata values ("${dataObject._name}", ${dataObject._version});`);
    }

    async function asyncForEach(arr, cb) {
        let i = 0;
        for (i = 0; i < arr.length; ++i) {
            await cb(arr[i], i, arr);
        }
    }

    module.exports.Field = Field;
    module.exports.Types = Types;
    module.exports.DataObjectCollection = DataObjectCollection;
    module.exports.Database = Database;
})();
const {Field, Types, Database, DataObjectCollection} = require("../main");

const idField = new Field("id", Types.Int);
idField.autoIncrement();
idField.primaryKey();

const UserInfo = new DataObjectCollection(
    "UserInfo",
    [
        idField,
        new Field("name", Types.String),
        new Field("age", Types.Int),
        new Field("father_name", Types.String)
    ],
    3
);

const TransactionInfo = new DataObjectCollection(
    "TransactionInfo",
    [
        idField,
        new Field("description", Types.Text)
    ]
);

const myDatabase = new Database(
        "test",
        [
            UserInfo,
            TransactionInfo
        ],
        {
            "user": "test",
            "password": "test",
            "host": "localhost"
        }
    );

myDatabase.getInstance();